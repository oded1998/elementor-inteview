#   DB CONF  #
CONSTRAINTS_NAMING = {
    "ix": "ix_%(column_0_label)s",
    "uq": "uq_%(table_name)s_%(column_0_name)s",
    "ck": "ck_%(table_name)s_%(constraint_name)s",
    "fk": "fk_%(table_name)s_%(column_0_name)s_%(referred_table_name)s",
    "pk": "pk_%(table_name)s"
}

#    VIRTUSTOAL CONF #
VIRTUSTOTAL_URL = "https://www.virustotal.com/vtapi/v2/url/report"

#    FILES CONF  #
SITES_TO_CHECK_RELATIVE_PATH = "sites_to_check.csv"

#   LOGGER_CONF #
LOGGER_FORMAT = "%(asctime)s [%(threadName)-12.12s] [%(levelname)-5.5s]  %(message)s"
