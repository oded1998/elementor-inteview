import os

from elmentor_interview.virtustotal_handler.virtustotal_api import VirtustotalApi
from elmentor_interview.virtustotal_handler.vulnerability_collector import VulnerabilityCollector

connection_string = os.getenv('CONNECTION_STRING')

virtustotal_key = os.getenv('VIRTUSTOTAL_KEY')
virtustotal_permium = os.getenv('VIRTUSTOTAL_PREMIUM', False)
virtustotal_api = VirtustotalApi(virtustotal_key, virtustotal_permium)
vulnerability_collector = VulnerabilityCollector(virtustotal_api)
