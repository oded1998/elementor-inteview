# Elementor Inteview

This project represents REST Api solution for URL theats problem.
The project is using Virtustotal Api to save the number of times any threat was analized by Cyber Security vendors.

Keep in mined that to power this REST application you will need to create **Virtustotal key** (preferably premium key),
and have DB ready to role :)


### How to run

Running the project is very simple
just follow these steps:

- clone the repository:
```sh
$ git clone https://gitlab.com/oded1998/elementor-inteview.git
```
- install the requirements
```sh
$ pip install -r requirements.txt
```
- **set Environment Variables** [https://stackoverflow.com/questions/42708389/how-to-set-environment-variables-in-pycharm](url):

**CONNECTION_STRING**- The DB connection string, for example using sql server:
`CONNECTION_STRING=mssql://<SERVER>/<DB>?trusted_connection=yes&driver=ODBC+Driver+13+for+SQL+Server`

**VIRTUSTOTAL_KEY**- Virtustotal API key. read more: [https://support.virustotal.com/hc/en-us/articles/115002088769-Please-give-me-an-API-key](url)

**VIRTUSTOTAL_PREMIUM**- If you are using Virtustotal premium, please set to True(for production- read more: [https://developers.virustotal.com/reference#public-vs-private-api](url))

-  Run the code :)



### REST Api docs
To access the REST api docs all you need to do is to adress _/docs_. for example in localhost:
`http://127.0.0.1:8080/docs`
