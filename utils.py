from typing import List, Dict
from conf import SITES_TO_CHECK_RELATIVE_PATH
from elmentor_interview.resources.schemas import WebsiteThreatCount, ScanResult, WebsiteScanResult, Website


def get_ds1_file(path: str = SITES_TO_CHECK_RELATIVE_PATH) -> List[str]:
    with open(path) as f:
        sites_to_check = f.read().splitlines()
    return sites_to_check


def website_threat_counter_list_to_scan_results(website_threat_counter_list: List[WebsiteThreatCount]) \
        -> List[ScanResult]:
    scan_result_list: List[ScanResult] = []
    for website_threat_counter in website_threat_counter_list:
        for status, counter in website_threat_counter.statuses.items():
            scan_result = ScanResult(url=website_threat_counter.website.url,
                                     url_id=website_threat_counter.website.id,
                                     check_time=website_threat_counter.website.last_check,
                                     status=status, counter=counter)
            scan_result_list.append(scan_result)
    return scan_result_list


def websites_scan_results_to_websites_threat_counter(websites_scan_results: List[WebsiteScanResult]) -> \
        List[WebsiteThreatCount]:
    websites_threat_counter_dict: Dict[int, WebsiteThreatCount] = {}
    for website_scan_result in websites_scan_results:
        if website_scan_result.website.id in websites_threat_counter_dict:
            websites_threat_counter_dict[website_scan_result.website.id].statuses[
                website_scan_result.scan_result.status] = website_scan_result.scan_result.counter
        else:
            websites_threat_counter_dict[website_scan_result.website.id] = WebsiteThreatCount(
                website=website_scan_result.website,
                statuses={website_scan_result.scan_result.status: website_scan_result.scan_result.counter})

    return list(websites_threat_counter_dict.values())
