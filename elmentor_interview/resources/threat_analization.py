from typing import List

from fastapi import APIRouter

from elmentor_interview.database.connector import session_scope
from elmentor_interview.database import queires
from elmentor_interview.resources.schemas import WebsiteThreatCount, Website, WebsiteScanResult, ScanResult
from factory import vulnerability_collector
from utils import website_threat_counter_list_to_scan_results, websites_scan_results_to_websites_threat_counter

website_threats_router = APIRouter(tags=['website-threats'])


@website_threats_router.put('/websites/threats', response_model=List[WebsiteThreatCount])
def activate_treat_update():
    with session_scope() as session:
        websites_to_check_list = [Website.from_orm(website) for website in queires.get_websites_to_check(session)]
        if not websites_to_check_list:
            return []
        website_threat_counter_list = vulnerability_collector.websites_report_status_counter(websites_to_check_list)
        scan_result_list = website_threat_counter_list_to_scan_results(website_threat_counter_list)
        queires.add_scan_results(session, scan_result_list)
        queires.update_websites_last_time_check(session,
                                                [website_threat_counter.website for website_threat_counter in
                                                 website_threat_counter_list])
    return website_threat_counter_list


@website_threats_router.get('/websites', response_model=List[Website])
def get_websites():
    with session_scope() as session:
        websites_list = queires.get_websites(session)
        return [Website.from_orm(website) for website in websites_list]


@website_threats_router.get('/websites/scan-results', response_model=List[WebsiteThreatCount])
def get_websites_scan_results():
    with session_scope() as session:
        websites_scan_results_list = [WebsiteScanResult(website=Website.from_orm(website_scan_result.Website),
                                                        scan_result=ScanResult.from_orm(website_scan_result.ScanResult))
                                      for
                                      website_scan_result in queires.get_websites_scan_results(session)]
        return websites_scan_results_to_websites_threat_counter(websites_scan_results_list)
