from typing import Union, Optional, Dict
from datetime import datetime

from pydantic import BaseModel


class Website(BaseModel):
    id: int
    url: str
    last_check: Union[None, datetime]

    class Config:
        orm_mode = True


class ScanResult(BaseModel):
    id: Optional[int]
    url_id: str
    check_time: datetime
    status: str
    counter: int

    class Config:
        orm_mode = True


class WebsiteScanResult(BaseModel):
    website: Website
    scan_result: ScanResult

    class Config:
        orm_mode = True


class WebsiteThreatCount(BaseModel):
    website: Website
    statuses: Dict[str, int]
