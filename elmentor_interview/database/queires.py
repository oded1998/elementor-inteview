from datetime import timedelta, datetime
from typing import List
from sqlalchemy import or_
from sqlalchemy.orm import Session
from elmentor_interview.database.models.elementor_site_reports import Website, ScanResult
from elmentor_interview.resources import schemas


def get_websites_to_check(session: Session):
    half_hour_ago = (datetime.now() - timedelta(minutes=30)).strftime('%Y-%m-%d %H:%M:%S')
    websites_orm_list = session.query(Website).filter(
        or_(Website.last_check < half_hour_ago, Website.last_check.is_(None))).all()
    return [schemas.Website.from_orm(website) for website in websites_orm_list]


def get_websites(session: Session):
    return session.query(Website).all()


def get_websites_scan_results(session: Session):
    return session.query(Website, ScanResult).join(ScanResult).filter(ScanResult.check_time == Website.last_check).all()


def update_websites_last_time_check(session: Session, websites_list: List[schemas.Website]) -> None:
    for website in websites_list:
        session.query(Website).filter(Website.id == website.id).update({Website.last_check: website.last_check})


def add_scan_results(session: Session, scan_result_list: List[schemas.ScanResult]) -> None:
    for scan_result in scan_result_list:
        session.add(
            ScanResult(url_id=scan_result.url_id, check_time=scan_result.check_time,
                       status=scan_result.status, counter=scan_result.counter))
