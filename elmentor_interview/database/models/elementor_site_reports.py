from sqlalchemy import Integer, Column, String, ForeignKey, MetaData, DateTime
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship

from conf import CONSTRAINTS_NAMING

meta = MetaData(naming_convention=CONSTRAINTS_NAMING)
Base = declarative_base(metadata=meta)


class Website(Base):
    __tablename__ = 'website'
    id = Column(Integer, primary_key=True, nullable=False)
    url = Column(String)
    last_check = Column(DateTime(timezone=True))

    def __repr__(self):
        return f"<Website(id={id}, fullname={url}, last_check={last_check})>"


class ScanResult(Base):
    __tablename__ = 'scan_result'
    id = Column(Integer, primary_key=True, nullable=False, autoincrement=True)
    url_id = Column(Integer, ForeignKey('website.id'))
    check_time = Column(DateTime(timezone=True))
    status = Column(String, nullable=False)
    counter = Column(Integer)
    website = relationship("Website", foreign_keys=[url_id])

    def __repr__(self):
        return f"<ScanResult(id={id}, url_id={url_id}, check_time={check_time}, status={status}, counter={counter})>"

