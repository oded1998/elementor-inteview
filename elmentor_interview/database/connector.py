from typing import List

import sqlalchemy
from sqlalchemy.orm import sessionmaker
from contextlib import contextmanager
from elmentor_interview.database.models.elementor_site_reports import Base, Website
from factory import connection_string

engine = sqlalchemy.create_engine(connection_string)
Session = sessionmaker(bind=engine)


@contextmanager
def session_scope():
    session = Session()
    try:
        yield session
        session.commit()
    except:
        session.rollback()
        raise
    finally:
        session.close()


def init_db(sites_to_check: List[str]) -> None:
    Base.metadata.create_all(engine)
    with session_scope() as session:
        site_already_in_db = [site.url for site in session.query(Website.url)]
        for site in sites_to_check:
            if site not in site_already_in_db:
                session.add(Website(url=site, last_check=None))
