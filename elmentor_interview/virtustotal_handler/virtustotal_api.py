import logging
import time
from typing import Dict

import requests
from requests import HTTPError

from conf import VIRTUSTOTAL_URL

logger = logging.getLogger(__name__)


class VirtustotalApi:
    def __init__(self, api_key: str, virtustotal_premium: bool):
        self._api_key = api_key
        self._virtustotal_premium = virtustotal_premium

    def get_website_report(self, website_url: str) -> Dict:
        try:
            if not self._virtustotal_premium:
                time.sleep(15)
            res = requests.get(VIRTUSTOTAL_URL, params={'apikey': self._api_key, 'resource': website_url})
            res.raise_for_status()
            return res.json()
        except HTTPError as e:
            logger.error(f'Failed to fetch data for {website_url} from virtustotal with error: {e}')
            raise
