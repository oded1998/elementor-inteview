import uvicorn
from fastapi import FastAPI

from elmentor_interview.database.connector import init_db
from elmentor_interview.resources.threat_analization import website_threats_router
from utils import get_ds1_file


def get_app() -> FastAPI:
    app = FastAPI()
    app.include_router(website_threats_router)
    return app


if __name__ == "__main__":
    sites_to_check = get_ds1_file()
    init_db(sites_to_check)
    uvicorn.run(get_app(), host='0.0.0.0', port=8080)
